const request = (url)=>{
   return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.timeout = 5000;
      xhr.onreadystatechange = () => {
         if (xhr.readyState === 4) {
            if (xhr.status === 200) {
               resolve(xhr.responseText);
            } else {
               reject(xhr.status);
            }
         }
      };
      xhr.ontimeout = () => {
         reject('Timeout');
      };
      xhr.open('GET', url);
      xhr.send();
   });
};