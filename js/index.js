const fillFormEl = (el, inpPlaceholder) => {
   el.html(`
<input type="text" class="form-control" placeholder=${inpPlaceholder} id=${'search' + inpPlaceholder} required style="max-width: 300px">
<button class="btn btn-info ml-2">Find</button>
`);
   $body.append(searchEl.append(el));
};

const createCardEl = (array) => {
   array.forEach((drink) => {
      const resultEl = $(`
               <div class="d-inline-block m-2 border rounded-1 myCard" style="min-width: 100px;max-width: 300px;cursor: pointer">
               <img src=${drink['strDrinkThumb']} alt="drink" style="width: 100%; height: auto">
               <p class="px-2">${drink['strDrink']}</p>
               </div>`);

      resultsEl
        .append(resultEl.append(createCardModalEL(drink)));

      resultEl.on('click', () => {
         $('#' + drink['idDrink']).modal();
      });
   });
   searchEl.append(resultsEl);
};

const createCardModalEL = (drink) => {
   const elseText = `
      <p><b>Instructions:</b> ${drink['strInstructions']}</p>
      <p><b>Glass:</b> ${drink['strGlass']}</p>
      <p><b>Category:</b> ${drink['strCategory']}</p>
      <p><b>Alcoholic:</b> ${drink['strAlcoholic']}</p>`;
   let ingredientsText = '';

   for (let i = 1; i < 15; i++) {
      if (!drink['strIngredient' + i]) {
         break;
      }
      let imgSrc = '';
      if (drink['strIngredient' + i].split(' ').length > 1) {
         imgSrc = `https://www.thecocktaildb.com/images/ingredients/${drink['strIngredient' + i].split(' ').join('%20')}-Small.png`;
      } else {
         imgSrc = `https://www.thecocktaildb.com/images/ingredients/${drink['strIngredient' + i]}-Small.png`;
      }

      ingredientsText += `
         <p><img src=${imgSrc} alt="drink"><span><i class="ingredientSearch">${drink['strIngredient' + i]}</i> ( ${drink['strMeasure' + i]} )</span></p>`
   }

   return $(`
      <div class="modal fade cardModal" id=${drink['idDrink']} tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
               <div class="modal-header">
                  <h5 class="modal-title">${drink['strDrink']}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
               </button>
               </div>
               <div class="modal-body">
               <h5>Ingredients:</h5>
               ${ingredientsText}
               ${elseText}
               </div>
               <div class="modal-footer">
                  <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
               </div>
            </div>
         </div>
      </div>`);
};

const createIngredientModal = () => {

   const ModalEl = (data, imgSrc) => {
      let ingredientText = ''
      Object.keys(data).forEach((key) => {
         if (data[key]) {
            key === 'idIngredient' ? ingredientText += `<p><b>Id:</b> ${data[key]}</p>` : ingredientText += `<p><b>${key.split('str').join('')}:</b> ${data[key]}</p>`;
         }
      })
      return $(`
      <div class="modal fade ingredientModal" id=${data['idIngredient']} tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg modal-dialog-centered modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title titleIngredient">${data['strIngredient']}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <img class="d-block mx-auto" src=${imgSrc} alt="drink">
       ${ingredientText}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary SearchByIngredient">Search</button>
      </div>
    </div>
  </div>
</div>`);
   }

   $('.ingredientSearch').on('click', (e) => {
        const el = $(e.currentTarget);
        const imgSrc = el.parent().siblings('img').attr('src').split('Small').join('Medium');
        request('https://www.thecocktaildb.com/api/json/v1/1/search.php?i=' + el.text())
          .then((response) => {
             const ingData = JSON.parse(response)['ingredients'][0];
             el.parents('.myCard').append(ModalEl(ingData, imgSrc));
             const modalEL = $('#' + ingData['idIngredient'])
             modalEL.modal();
             modalEL.on('hidden.bs.modal', () => {
                modalEL.remove();
             })
             $('.SearchByIngredient').on('click', (e) => {
                const el = $(e.currentTarget);
                $('#searchIngredient').val(el.parents('.modal-content').find('.titleIngredient').text());
                modalEL.modal('hide');
                modalEL.parents('.myCard').find('.cardModal').modal('hide').on('hidden.bs.modal', () => {
                   formIngredientEl.trigger('submit');
                });
             })
          })
          .catch((error) => {
             console.log(error);
          })
     }
   )
}

const $body = $('body');
const searchEl = $('<div>');
const formCocktailEl = $('<form class="mt-4 mx-3 d-inline-flex">');
fillFormEl(formCocktailEl, 'Cocktail');
const formIngredientEl = $('<form class="mt-4 mx-3 d-inline-flex">');
fillFormEl(formIngredientEl, 'Ingredient');
const resultsEl = $(`<div class="d-flex flex-wrap justify-content-center my-3 p-2">`);

formCocktailEl.on('submit', (e) => {
   e.preventDefault();
   resultsEl.empty();
   request('https://www.thecocktaildb.com/api/json/v1/1/search.php?s=' + $('#searchCocktail').val().toLowerCase())
     .then(response => {
        createCardEl(JSON.parse(response)['drinks']);
        createIngredientModal();
     })
     .catch((error) => {
        console.log(error);
     });
});

formIngredientEl.on('submit', (e) => {
   e.preventDefault();
   resultsEl.empty();
   request('https://www.thecocktaildb.com/api/json/v1/1/filter.php?i=' + $('#searchIngredient').val().toLowerCase())
     .then((response) => {
        return JSON.parse(response)['drinks'];
     })
     .then((response) => {
        return Promise.all(response.map((drink) => {
           return request('https://www.thecocktaildb.com/api/json/v1/1/lookup.php?i=' + drink['idDrink'])
        }));
     })
     .then((arrayStr) => {
        return arrayStr
          .map((drinkStr) => {
             return JSON.parse(drinkStr);
          })
          .map((drink) => {
             return drink['drinks'][0];
          });
     })
     .then((drinks) => {
        createCardEl(drinks);
        createIngredientModal();
     })
     .catch((error) => {
        console.log(error);
     })
});


